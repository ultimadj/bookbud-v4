'use strict';

angular.module('bookbudApp')
  .controller('NavCtrl', ["$scope", "$location", "$rootScope", "$log", "AuthSvc", function ($scope, $location, $rootScope, $log, authSvc) {
    $scope.authSvc = authSvc;
    $scope.logout = authSvc.logout;
    $scope.updateNav = function() {
    	$scope.navActive = $location.path().substring(1) || 'home';
        $scope.showLogin = !$scope.authSvc.isLoggedIn();
        if($scope.showLogin) {
            $scope.name = null;
            $scope.navItems = [
                {'name':'Home', "url":"#/"}
            ];
        } else {
            $scope.name = authSvc.getPrinciple().name;
            $scope.navItems = [
                {'name':'Home', "url":"#/", active: 'home' == $scope.navActive},
                {'name':'Library', "url":"#/library", active: 'library' == $scope.navActive}
            ];
        }
        $log.log('Add Book'.replace(/\s+/g, '').toLowerCase() + " " + $scope.navActive.toLowerCase())
    };
    $scope.updateNav();
    
    // register listener to watch route changes
    $rootScope.$on( "$routeChangeSuccess", function(event, next, current) {
      $scope.updateNav();
    });
  }]);
