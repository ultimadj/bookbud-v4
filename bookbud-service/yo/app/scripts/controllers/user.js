'use strict';

angular.module('bookbudApp')
    .controller('UserCtrl', ["$scope", "$http", "$cookieStore", "$log", "$resource", function ($scope, $http, $cookieStore, $log, $resource) {
        $scope.user = {name: "", email: "", id: ""}
        $http.get("/rest/user").success(function(data, status, headers, config) {
                if(data.errorMessage != "") {
                    $scope.errMsg = data.errorMessage;
                    $log.log("Error: " + angular.toJson(data));
                } else {
                    $scope.errMsg = null;
                    $scope.user = {name: data.user.name, email: data.user.email, id: data.user.id};
                    $log.log("Updated user from service response: " + angular.toJson(data));
                }
            }
        ).error(function(data, status, headers, config) {

            }
        );
        $scope.resetSandboxData = function() {
            $http.get("rest/init").success(function() {
                $scope.showReset = true;
            });
        };
        $scope.closeUpdate = function() {
            $scope.showReset = false;
        };

        $scope.prepareDownload = function() {
            $http.get("rest/library/export").success(function(data, status, headers, config) {
                var blob = new Blob([angular.toJson(data, true)], {type: 'application/json'});
                $scope.exportUrl = (window.URL || window.webkitURL).createObjectURL(blob);
                $scope.showDownload = true;
            });
        }
    }]
);
