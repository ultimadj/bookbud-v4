/**
 * Created by David on 3/17/14.
 */
'use strict';

angular.module('bookbudApp')
    .controller('ModalOkCancelCtrl', ["$scope", "$modalInstance", "modal", function ($scope, $modalInstance, modal) {
        $scope.modal = modal;
        $scope.ok = function() {
            $modalInstance.close(modal.data);
        };
        $scope.cancel = function() {
            $modalInstance.dismiss();
        };
    }]
);