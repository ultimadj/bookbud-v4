'use strict';

angular.module('bookbudApp')
  .controller('MainCtrl', ["$scope", "AuthSvc", function ($scope, authSvc) {
    $scope.showLogin = !authSvc.isLoggedIn();
  }]
);
