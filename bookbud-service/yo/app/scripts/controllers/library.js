'use strict';

angular.module('bookbudApp').controller('LibraryCtrl',
    ["$scope", "$http", "$modal", "$log", "LibrarySvc", function ($scope, $http, $modal, $log, librarySvc) {
    $scope.refresh = function () {
        librarySvc.getLibaryOverview(
            function (data) {
                $scope.libraryOverview = data;
            }
        );
    };
    $scope.deleteBook = function (bookID) {
        librarySvc.deleteBook(bookID, function (data) {
            $scope.refresh();
        });
    };
//    $scope.deleteBookPrompt = function (book) {
//        $scope.showDeletePrompt = true;
//        $scope.bookToDelete = book;
//    };
    $scope.refresh();
    $scope.bookFilter = function (item) {
        return !$scope.search
            || item.title.toLowerCase().indexOf($scope.search.toLowerCase()) >= 0
            || item.author.toLowerCase().indexOf($scope.search.toLowerCase()) >= 0;
    };

    $scope.showDelete = function (book) {

        var modalInstance = $modal.open({
            templateUrl: 'template/bbModal.html',
            controller: 'ModalOkCancelCtrl',
            resolve: {
                modal: function () {
                    return {data: book.id, title: "Confirm Delete", text: 'Are you sure you want to delete "' + book.title + '"?'};
                }
            }
        });

        modalInstance.result.then(function (bookID) {
            $scope.deleteBook(bookID);
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };
}]
);