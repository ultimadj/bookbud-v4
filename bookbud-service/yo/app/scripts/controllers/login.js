'use strict';

angular.module('bookbudApp')
    .controller('LoginCtrl', ["$scope", "AuthSvc", function ($scope, authSvc) {
        $scope.login = function() {
            authSvc.login($scope.username, $scope.password, function(user, authResponse) {
                $scope.token = authResponse.token;
                $scope.errMsg = null;
            }, function(user, errorMessage) {
                $scope.errMsg = errorMessage;
            });
        };

        $scope.sandboxLogin = function() {
            authSvc.login("a@b.com", "test", function(user, authResponse) {
                $scope.token = authResponse.token;
                $scope.errMsg = null;
            }, function(user, errorMessage) {
                $scope.errMsg = errorMessage;
            });
        };
    }]
);
