'use strict';

angular.module('bookbudApp').controller('BookCtrl', ["$scope", "$http", "LibrarySvc", "$routeParams", "$location", "$timeout", "$log", "$anchorScroll", function ($scope, $http, librarySvc, $routeParams, $location, $timeout, $log, $anchorScroll) {
        $scope.alerts = [];
        $scope.tags = [""];

        $scope.doGetBook = function() {
            librarySvc.getBook($routeParams.id, function(data) {
                $scope.book = data;
                $scope.modifiedDate = new Date(data.modifiedDate)
                $scope.createdDate = new Date(data.createdDate)
            });
        };

        $scope.swapTitle = function() {
            if($scope.editTitle) {
                $scope.editTitle = false;
                $scope.save();
            } else $scope.editTitle = true;
        };

        if($routeParams.id) {
            $scope.doGetBook();
        } else {
            $scope.book = {title: "", author: "", notes: "", id: null}
            $scope.swapTitle();
        }


        $scope.cancel = function() {
            $scope.alerts = [];
            $location.path("/library");
            $anchorScroll();
        };

        $scope.save = function() {
            $log.log("Preparing to save book:");
            $log.log($scope.book);
            librarySvc.saveBook($scope.book, function(data, status) {
               $scope.alerts = [];
               $scope.showSaved = true;
               $timeout(function() {
                   $scope.showSaved = false;
                   $location.path("/library");
                   $anchorScroll();
               }, 1300);
               $scope.book = data;
            });
        };

        $scope.saveNoNav = function() {
            $log.log("Preparing to save book:");
            $log.log($scope.book);
            librarySvc.saveBook($scope.book, function(data, status) {
                $scope.alerts = [];
                $scope.showSaved = true;
                $timeout(function() {
                    $scope.showSaved = false;
                }, 1300);
                $scope.book = data;
            });
        }

        /*
        Tag editing
         */
        $scope.$watch('book', function() {
            if($scope.book) $scope.tags = $scope.book.tags;
        });
        $scope.addTag = function() {
            if(!$scope.newTag) return;
            if(!$scope.book.tags) $scope.book.tags = [];
            $scope.book.tags.push($scope.newTag);
            $scope.book.tags.sort(
                function (a, b) {
                    return a.toLowerCase().localeCompare(b.toLowerCase());
                }
            );
            $scope.newTag = "";
            $scope.tags = $scope.book.tags;
        };
        $scope.deleteTag = function(tag) {
            if(!$scope.book.tags) return;
            var idx = $scope.book.tags.indexOf(tag);
            if(idx > -1) $scope.book.tags.splice(idx, 1);
            $scope.tags = $scope.book.tags;
        }
    }]
);