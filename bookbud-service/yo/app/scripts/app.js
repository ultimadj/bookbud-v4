'use strict';

angular.module('bookbudApp', ["ngResource", "ngRoute", "ngCookies", "ui.bootstrap"])
    .config(["$routeProvider", "$compileProvider", function ($routeProvider, $compileProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'views/main.html',
                controller: 'MainCtrl'
            })
            .when('/library', {
                templateUrl: 'views/library.html',
                controller: 'LibraryCtrl'
            })
            .when('/library/addBook', {
                templateUrl: 'views/book.html',
                controller: 'BookCtrl'
            })
            .when('/library/book/:id', {
                templateUrl: 'views/book.html',
                controller: 'BookCtrl'
            })
            .when('/user', {
                templateUrl: 'views/user.html',
                controller: 'UserCtrl'
            })
            .when('/login', {
                templateUrl: 'views/login.html',
                controller: 'LoginCtrl'
            })
            .otherwise({
                redirectTo: '/'
            });

        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https|blob|mailto|http):/);
    }]);