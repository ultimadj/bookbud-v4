angular.module('bookbudApp').directive('bbTags', function() {
    /*
    Required:
    $scope.

    tags
    addTag <- uses $scope.newtag
    deleteTag(tag)
    saveNoNav()
     */
    return {
        templateUrl: 'template/bbTags.html'
    };
});