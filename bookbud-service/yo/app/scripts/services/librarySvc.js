var bookbud = angular.module('bookbudApp');
bookbud.factory('LibrarySvc', ['$http', function($http) {
    return {
        getLibaryOverview: function(callback) {
            $http.get("rest/library").success(
                function(data, status, headers, config) {
                    console.log("Library overview: " + status);
                    console.log(data);
                    if(status == 200) {
                        callback(data);
                    }
                }
            );
        },
        getBook: function(bookId, callback) {
            $http.get("rest/library/book/" + bookId).success(
                function(data, status, headers, config) {
                    console.log("Book: " + status);
                    console.log(data);
                    if(status == 200) {
                        callback(data);
                    }
                }
            );
        },
        saveBook: function(book, callback) {
            console.log("About to save book:")
            console.log(book);
            $http.post("rest/library/book/", book).success(
                function(data, status, headers, config) {
                    callback(data, status);
                }
            );
        },
        deleteBook: function(bookID, callback) {
            console.log("Deleting book: " + bookID);
            $http.delete("rest/library/book/" + bookID).success(
                function(data, status, headers, config) {
                    callback(data);
                }
            )
        }
    }
}]);