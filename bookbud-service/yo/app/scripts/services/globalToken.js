var bookbud = angular.module('bookbudApp');
bookbud.config(["$httpProvider", function($httpProvider, $cookieStore) {
    $httpProvider.interceptors.push(["$q", "$cookieStore", "$location", "$log", "$anchorScroll", function($q, $cookieStore, $location, $log, $anchorScroll) {
        return {
            'request': function(config) {
                var authResponse = $cookieStore.get("auth");
                if(null != authResponse) {
                    $log.log("URL: " + config.url);
                    $log.log(config);
                    config.url = URI(config.url).addSearch({'token':authResponse.token}).toString();
                }
                return config;
            },
            'responseError': function(response) {
                // Forbidden or Unauthorized
                if(response.status == 401 || response.status == 403) {
                    console.log("Received rejection. Redirect to login.")
                    $location.path("login");
                    $anchorScroll();
                }
                return response;
            }
        };
    }]);
}]);