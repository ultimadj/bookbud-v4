var bookbud = angular.module('bookbudApp');
bookbud.factory('AuthSvc', ['$http', "$cookieStore", "$location", "$anchorScroll", function($http, $cookieStore, $location, $anchorScroll) {
    var doLogin = function(user, password, callback, errorCallback) {
        console.log("Requesting auth token with: " + user + ", " + password);
        $http.post("rest/auth", {user: user, pass: password}).success(
            function(data, status, headers, config) {
                console.log("Login response: Code: " + status + " Data: ");
                console.log(data);
                if(status != 200) {
                    errorCallback(user, "Unable to log in.");
                } else if(!data.success) {
                    errorCallback(user, "Invalid username/password.");
                } else {
                    // this callback will be called asynchronously
                    // when the response is available
                    $cookieStore.put("auth", data);
                    callback(user, data);
                    $location.path('library')
                    $anchorScroll();
                }
            }
        ).error(
            function(data, status, headers, config) {
                // this callback will be called asynchronously
                // when the response is available
                console.log("err - Login response: Code: " + status + " Data: ");
                console.log(data);
                errorCallback(user, "Unable to log in.");
            }
        );
    };
    var doLogout = function() {
        $cookieStore.remove("auth");
    }
    return {
        login: function(user, pass, callback, errorCallback) {
            doLogin(user, pass, callback, errorCallback);
        },
        isLoggedIn: function() {
            var authToken = $cookieStore.get("auth");
            return  authToken != null && authToken.success;
        },
        logout: function() {
            doLogout();
        },
        getPrinciple: function() {
            return $cookieStore.get("auth");
        }
    }
}]);