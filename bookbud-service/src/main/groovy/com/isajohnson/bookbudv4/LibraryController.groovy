package com.isajohnson.bookbudv4

import com.isajohnson.bookbudv4.model.Book
import com.isajohnson.bookbudv4.model.Library
import com.isajohnson.bookbudv4.model.message.ExportLibraryResponse
import com.isajohnson.bookbudv4.model.message.LibraryResponse
import com.isajohnson.bookbudv4.repositories.BookRepository
import com.isajohnson.bookbudv4.repositories.LibraryRepository
import com.isajohnson.bookbudv4.security.TokenAuthentication
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.core.context.SecurityContext
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.ResponseBody

/**
 * Created by David on 3/7/14.
 */
@Controller
@RequestMapping("/rest/library")
class LibraryController {
    Logger log = LoggerFactory.getLogger(LibraryController.class)
    @Autowired LibraryRepository libraryRepository
    @Autowired BookRepository bookRepository

    Library retrieveOrCreateLibraryForCurrentUser() {
        SecurityContext context = SecurityContextHolder.getContext()
        String userId = ((TokenAuthentication)context.authentication)?.authToken?.userId;
        if(!userId) return null
        Library library = libraryRepository.findByUserId(userId)
        if(!library) {
            library = new Library(userId: userId)
            libraryRepository.save(library)
        }

        return library
    }

    @RequestMapping @ResponseBody
    LibraryResponse currentUserLibrary() {
        Library library = retrieveOrCreateLibraryForCurrentUser()
        if(!library) return new LibraryResponse(errorMessage: "Unable to retrieve library. Please make sure you're logged in.")
        List<Book> books = bookRepository.findByLibraryId(library.id)
        LibraryResponse response = new LibraryResponse()
        List<LibraryResponse.LibraryBook> libraryBooks = []
        for(Book book : books) {
            libraryBooks.add(new LibraryResponse.LibraryBook(
                    id: book.id,
                    author: book.author,
                    title: book.title,
                    tagCount: book.tags?.size() ?: 0
            ))
        }
        libraryBooks = libraryBooks.sort ({a,b ->
            a.title.toLowerCase() <=> b.title.toLowerCase()
        })
        response.books = libraryBooks
        return response
    }

    @RequestMapping(value = "/book/{id}", method = RequestMethod.GET) @ResponseBody
    ResponseEntity<Book> retrieveBook(@PathVariable String id) {
        Book book = bookRepository.findOne(id)
        if(!book) return new ResponseEntity<Book>(HttpStatus.NOT_FOUND)
        return new ResponseEntity<Book>(book, HttpStatus.OK)
    }

    @RequestMapping(value = "/book/{id}", method = RequestMethod.DELETE) @ResponseBody
    ResponseEntity<Book> deleteBook(@PathVariable String id) {
        Book book = bookRepository.findOne(id)
        if(!book) return new ResponseEntity<Book>(HttpStatus.NOT_FOUND)
        bookRepository.delete(book)
        return new ResponseEntity<Book>(book, HttpStatus.OK)
    }

    @RequestMapping(value = "/book", method = RequestMethod.POST) @ResponseBody
    ResponseEntity<Book> createOrUpdateBook(@RequestBody Book book) {
        Library currentLibrary = retrieveOrCreateLibraryForCurrentUser()

        log.info("Received: {}", book.toString())
        if(book.id) {
            Book existing = bookRepository.findOne(book.id)
            Library library = libraryRepository.findOne(existing.libraryId)

            SecurityContext context = SecurityContextHolder.getContext()
            String userId = ((TokenAuthentication)context.authentication)?.authToken?.userId
            if(library.userId != userId) {
                return new ResponseEntity<Book>(book, HttpStatus.FORBIDDEN)
            }
        } else {
            book.setLibraryId(currentLibrary.id)
        }
        book.modifiedDate = new Date()
        if(!book.createdDate) book.createdDate = new Date()
        bookRepository.save(book)
        return new ResponseEntity<Book>(book, HttpStatus.OK)
    }

    @RequestMapping(value = "/export", method = RequestMethod.GET) @ResponseBody
    ResponseEntity<ExportLibraryResponse> exportLibrary() {
        Library currentLibrary = retrieveOrCreateLibraryForCurrentUser()
        if(!currentLibrary) {
            return new ResponseEntity<ExportLibraryResponse>(null, HttpStatus.NOT_FOUND)
        }

        ExportLibraryResponse exportLibraryResponse = new ExportLibraryResponse(library: currentLibrary)
        exportLibraryResponse.books = bookRepository.findByLibraryId(currentLibrary.id)
        exportLibraryResponse.ideas = [] //TODO add ideas to export
        return new ResponseEntity<ExportLibraryResponse>(exportLibraryResponse, HttpStatus.OK)
    }
}
