package com.isajohnson.bookbudv4.security

import com.isajohnson.bookbudv4.model.AuthToken
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.Authentication
import org.springframework.security.core.AuthenticationException
import org.springframework.security.core.context.SecurityContext
import org.springframework.security.core.context.SecurityContextHolder

import javax.servlet.Filter
import javax.servlet.FilterChain
import javax.servlet.FilterConfig
import javax.servlet.ServletException
import javax.servlet.ServletRequest
import javax.servlet.ServletResponse

/**
 * Created by David on 3/1/14.
 */
class AuthenticationTokenFilter implements Filter {
    Logger log = LoggerFactory.getLogger(AuthenticationTokenFilter.class);
    @Autowired TokenAuthenticationProvider provider;

    @Override
    void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        SecurityContext context = SecurityContextHolder.getContext()

        String[] tokenVals = request.getParameterValues("token")
        String token = null
        if(tokenVals && tokenVals.length > 0) token = tokenVals[0]
        if(token) {
            Authentication authentication = new TokenAuthentication(authToken: new AuthToken(token: token))
            try {
                provider.authenticate(authentication)
                context.setAuthentication(authentication)
            } catch (AuthenticationException e) {
                context.setAuthentication(null)
            }
        } else {
            context.setAuthentication(null)
        }

//        if(context.getAuthentication()?.isAuthenticated()) {
//            log.debug("Already authenticated.")
//        } else {
//            String[] tokenVals = request.getParameterValues("token")
//            String token = null
//            if(tokenVals && tokenVals.length > 0) token = tokenVals[0]
//            if(token) {
//                Authentication authentication = new TokenAuthentication(token: token)
//                provider.authenticate(authentication);
//                context.setAuthentication(authentication)
//            }
//        }
        chain.doFilter(request, response)
    }

    @Override
    void destroy() {
    }
}
