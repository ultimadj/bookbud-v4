package com.isajohnson.bookbudv4.security

import com.isajohnson.bookbudv4.model.AuthToken
import com.isajohnson.bookbudv4.repositories.AuthTokenRepository
import com.isajohnson.bookbudv4.repositories.UserRepository
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.core.Authentication
import org.springframework.security.core.AuthenticationException

/**
 * Created by David on 3/1/14.
 */
class TokenAuthenticationProvider implements AuthenticationProvider {
    @Autowired AuthTokenRepository authTokenRepository

    Logger log = LoggerFactory.getLogger(TokenAuthenticationProvider.class);

    @Override
    Authentication authenticate(Authentication authentication) throws AuthenticationException {
        if(authentication.isAuthenticated()) return authentication;

        String token = authentication.getCredentials().toString();
        AuthToken authToken = authTokenRepository.findByToken(token);

        //http://stackoverflow.com/questions/19791406/spring-security-authentication-based-on-request-parameter
        if(authToken && authToken.expiration > new Date()) {
            authentication.setAuthenticated(true)
        } else {
            throw new BadCredentialsException("No token");
        }

        ((TokenAuthentication)authentication).authToken = authToken
        return authentication
    }

    @Override
    boolean supports(Class<?> authentication) {
        return authentication.equals(AuthToken.class)
    }
}
