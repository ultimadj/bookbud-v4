package com.isajohnson.bookbudv4.security

import com.isajohnson.bookbudv4.model.AuthToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.GrantedAuthority

/**
 * Created by David on 3/1/14.
 */
class TokenAuthentication implements Authentication {
    AuthToken authToken
    boolean authenticated

    @Override
    Object getPrincipal() {
        return authToken
    }

    @Override
    String getName() {
        return authToken?.token ?: "";
    }

    @Override
    Collection<? extends GrantedAuthority> getAuthorities() {
        return new LinkedList<GrantedAuthority>()
    }

    @Override
    Object getCredentials() {
        return authToken.token
    }

    @Override
    Object getDetails() {
        return authToken
    }

    @Override
    boolean isAuthenticated() {
        return authenticated
    }

    @Override
    void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
        authenticated = isAuthenticated
    }
}
