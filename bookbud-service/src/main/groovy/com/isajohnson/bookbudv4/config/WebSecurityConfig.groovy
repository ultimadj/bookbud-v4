package com.isajohnson.bookbudv4.config

import com.isajohnson.bookbudv4.security.AuthenticationTokenFilter
import com.isajohnson.bookbudv4.security.TokenAuthenticationProvider
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter

/**
 * Created by David on 2/25/14.
 */
@Configuration
@EnableWebMvcSecurity
class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired TokenAuthenticationProvider tokenAuthenticationProvider
    @Autowired AuthenticationTokenFilter authenticationTokenFilter

    @Bean TokenAuthenticationProvider tokenAuthenticationProvider() {
        new TokenAuthenticationProvider()
    }

    @Bean AuthenticationTokenFilter authenticationTokenFilter() {
        new AuthenticationTokenFilter()
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
        .antMatchers("/rest/auth").permitAll()
        .antMatchers("/rest/init").permitAll()
        .antMatchers("/rest/**").authenticated()
        .anyRequest().permitAll()
        .and()
        .addFilterBefore(authenticationTokenFilter, BasicAuthenticationFilter.class)

        http.csrf().disable()
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(tokenAuthenticationProvider)
    }


}
