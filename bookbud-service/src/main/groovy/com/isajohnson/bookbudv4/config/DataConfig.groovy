package com.isajohnson.bookbudv4.config

import com.mongodb.Mongo
import org.springframework.context.annotation.Configuration
import org.springframework.data.mongodb.config.AbstractMongoConfiguration
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories

/**
 * Created by David on 3/5/14.
 */
@Configuration
@EnableMongoRepositories
class DataConfig extends AbstractMongoConfiguration {
    @Override
    protected String getDatabaseName() {
        return "bookbudv4"
    }

    @Override
    Mongo mongo() throws Exception {
        return new Mongo()
    }
}
