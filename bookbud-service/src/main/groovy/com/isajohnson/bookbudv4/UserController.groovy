package com.isajohnson.bookbudv4

import com.isajohnson.bookbudv4.model.AuthToken
import com.isajohnson.bookbudv4.model.User
import com.isajohnson.bookbudv4.model.message.UserResponse
import com.isajohnson.bookbudv4.repositories.AuthTokenRepository
import com.isajohnson.bookbudv4.repositories.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody

/**
 * Created by David on 2/25/14.
 */
@Controller
@RequestMapping("/rest/user")
class UserController {
    @Autowired AuthTokenRepository authTokenRepository
    @Autowired UserRepository userRepository

    @RequestMapping
    public @ResponseBody UserResponse currentUser(String token) {
        UserResponse userResponse = new UserResponse(errorMessage: "Unable to find user.");
        AuthToken authToken = authTokenRepository.findByToken(token);
        if(!authToken) return userResponse;
        User user = userRepository.findOne(authToken.userId);
        if(!user) return userResponse

        userResponse.errorMessage = ""
        userResponse.user = user
        return userResponse
    }
}
