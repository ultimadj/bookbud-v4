package com.isajohnson.bookbudv4

import com.isajohnson.bookbudv4.model.Book
import com.isajohnson.bookbudv4.model.Library
import com.isajohnson.bookbudv4.model.message.InitResponse
import com.isajohnson.bookbudv4.model.User
import com.isajohnson.bookbudv4.repositories.BookRepository
import com.isajohnson.bookbudv4.repositories.IdeaRepository
import com.isajohnson.bookbudv4.repositories.LibraryRepository
import com.isajohnson.bookbudv4.repositories.UserRepository
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody

/**
 * Created by David on 3/5/14.
 */
@Controller
@RequestMapping("/rest/init")
class InitController {
    Logger log = LoggerFactory.getLogger(InitController.class)
    @Autowired UserRepository userRepository
    @Autowired BookRepository bookRepository
    @Autowired IdeaRepository ideaRepository
    @Autowired LibraryRepository libraryRepository

    @RequestMapping @ResponseBody
    InitResponse init() {
        log.info("Application: Init called.")
        User user = userRepository.findByEmail("ultimadj@gmail.com")
        if(!user) {
            user = new User(
                    name: "super",
                    email: "ultimadj@gmail.com",
                    encryptedPassword: "rtij0VokWXLKzr/IdxXDSx8IJhdm5Wws1sJtE21GZq2F7rtFo8DJ9MYsxpfm4obw"
            )
            userRepository.save(user)
        }

        User cheryl = userRepository.findByEmail("teachercheryl@gmail.com")
        if(!cheryl)cheryl = new User()
        cheryl.name = "Cheryl"
        cheryl.email = "teachercheryl@gmail.com"
        cheryl.encryptedPassword = "G6CCwMm/b8TUZR4JU+l3NKd+IBPNRxLa0P3b3SQBnrPuFQqwnoWkN1mDUbg8IGoE"
        userRepository.save(cheryl)

        user = userRepository.findByEmail("a@b.com")
        if(!user) {
            user = new User(
                    name: "Sandbox",
                    email: "a@b.com",
                    // "test"
                    encryptedPassword: "OAC/07tlVmuHL9SmuiKiPiD5rEuubnZl7jx9JbIEEPwX93/+rnmDTOfVbUx8FerZ"
            )
            userRepository.save(user)
        }

        initSandboxData(user)


        return new InitResponse(message: "Users successfully initialized!");
    }

    void initSandboxData(User user) {
        Library sandboxLibrary = libraryRepository.findByUserId(user.id)
        if(!sandboxLibrary) {
            sandboxLibrary = new Library(userId: user.id)
            libraryRepository.save(sandboxLibrary)
        }

        List<Book> sandboxBooks = bookRepository.findByLibraryId(sandboxLibrary.id)
        sandboxBooks.each {bookRepository.delete(it)}
        bookRepository.save([
                new Book(
                        title: "Ender's Game",
                        author: "Orson Scott Card",
                        libraryId: sandboxLibrary.id,
                        notes: "Best book ever!!",
                        createdDate: new Date(),
                        tags: ["good", "fun"],
                        modifiedDate: new Date()
                ),
                new Book(
                        title: "Xenocide",
                        author: "Orson Scott Card",
                        libraryId: sandboxLibrary.id,
                        notes: "Another good one.",
                        createdDate: new Date(),
                        tags: ["good", "fun", "dark"],
                        modifiedDate: new Date()
                ),
                new Book(
                        title: "Belle's Book for Awesome People",
                        author: "One Cool Guy",
                        libraryId: sandboxLibrary.id,
                        notes: "Tales for Tots and such...",
                        createdDate: new Date(),
                        tags: ["belle"],
                        modifiedDate: new Date()
                ),
                new Book(
                        title: "You Wouldn't Want to Be a Civil War Soldier",
                        author: "Thomas Ratliff",
                        libraryId: sandboxLibrary.id,
                        notes: "Funny details about what was not fun or gruesome or awful about that life.",
                        tags: ["ss", "history", "civil war"]
                )
        ])
    }
}
