package com.isajohnson.bookbudv4.repositories

import com.isajohnson.bookbudv4.model.Book
import com.isajohnson.bookbudv4.model.Idea
import org.springframework.data.mongodb.repository.MongoRepository

/**
 * Created by David on 3/7/14.
 */
public interface IdeaRepository extends MongoRepository<Idea, String> {
    List<Idea> findByLibraryId(String libraryId)
}