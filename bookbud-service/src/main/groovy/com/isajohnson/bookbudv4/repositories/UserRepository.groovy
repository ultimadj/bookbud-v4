package com.isajohnson.bookbudv4.repositories

import com.isajohnson.bookbudv4.model.User
import org.springframework.data.mongodb.repository.MongoRepository

/**
 * Created by David on 3/5/14.
 */
interface UserRepository extends MongoRepository<User, String> {
    User findByEmail(String email)
}
