package com.isajohnson.bookbudv4.repositories

import com.isajohnson.bookbudv4.model.Book
import com.isajohnson.bookbudv4.model.User
import org.springframework.data.mongodb.repository.MongoRepository

/**
 * Created by David on 3/7/14.
 */
public interface BookRepository extends MongoRepository<Book, String> {
    List<Book> findByLibraryId(String libraryId)
}