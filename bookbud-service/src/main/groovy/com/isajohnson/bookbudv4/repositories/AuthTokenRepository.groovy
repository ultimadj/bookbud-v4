package com.isajohnson.bookbudv4.repositories

import com.isajohnson.bookbudv4.model.AuthToken
import com.isajohnson.bookbudv4.model.User
import org.springframework.data.mongodb.repository.MongoRepository

/**
 * Created by David on 3/5/14.
 */
interface AuthTokenRepository extends MongoRepository<AuthToken, String> {
    AuthToken findByToken(String token)
    List<AuthToken> findByExpirationLessThan(Date currentTime)
}
