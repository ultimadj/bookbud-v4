package com.isajohnson.bookbudv4.repositories

import com.isajohnson.bookbudv4.model.Idea
import com.isajohnson.bookbudv4.model.Library
import org.springframework.data.mongodb.repository.MongoRepository

/**
 * Created by David on 3/7/14.
 */
public interface LibraryRepository extends MongoRepository<Library, String> {
    Library findByUserId(String userId)
}