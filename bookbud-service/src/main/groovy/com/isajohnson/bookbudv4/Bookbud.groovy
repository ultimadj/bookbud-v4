package com.isajohnson.bookbudv4

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.context.annotation.ComponentScan

/**
 * Created by David on 2/25/14.
 */
@ComponentScan
@EnableAutoConfiguration
class Bookbud {
    static void main(String[] args) {
        SpringApplication.run(Bookbud.class, args)
    }
}
