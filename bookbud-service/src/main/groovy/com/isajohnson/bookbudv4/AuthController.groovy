package com.isajohnson.bookbudv4

import com.isajohnson.bookbudv4.model.message.AuthRequest
import com.isajohnson.bookbudv4.model.message.AuthResponse
import com.isajohnson.bookbudv4.model.AuthToken
import com.isajohnson.bookbudv4.model.User
import com.isajohnson.bookbudv4.repositories.AuthTokenRepository
import com.isajohnson.bookbudv4.repositories.UserRepository
import org.jasypt.util.password.StrongPasswordEncryptor
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody

import java.security.SecureRandom

/**
 * Created by David on 3/1/14.
 */
@Controller
@RequestMapping("/rest/auth")
class AuthController {
    static final int DEFAULT_TIMEOUT = 172800

    @Autowired UserRepository userRepository
    @Autowired AuthTokenRepository authTokenRepository

    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody AuthResponse auth(@RequestBody AuthRequest authRequest) {
        User user = userRepository.findByEmail(authRequest.user)
        if(!user) return new AuthResponse(success: false);

        StrongPasswordEncryptor strongPasswordEncryptor = new StrongPasswordEncryptor()
        if(!strongPasswordEncryptor.checkPassword(authRequest.pass, user.encryptedPassword)) return new AuthResponse(success: false);

        Calendar calendar = Calendar.getInstance()
        calendar.add(Calendar.SECOND, DEFAULT_TIMEOUT);
        AuthToken authToken = new AuthToken(
                token: new SecureRandom().nextLong(),
                expiration: calendar.getTime(),
                userId: user.id
        )
        authTokenRepository.save(authToken)

        /*
        Housekeeping. Clear out expired tokens
         */
        List<AuthToken> expired = authTokenRepository.findByExpirationLessThan(new Date());
        for(AuthToken expiredToken : expired) authTokenRepository.delete(expired)

        return new AuthResponse(
                token: authToken.token,
                success: true,
                secondsTilExpiration: DEFAULT_TIMEOUT,
                name: user.name
        )
    }
}
