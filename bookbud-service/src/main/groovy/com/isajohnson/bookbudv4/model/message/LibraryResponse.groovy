package com.isajohnson.bookbudv4.model.message

/**
 * Created by David on 3/7/14.
 */
class LibraryResponse {
    static class LibraryBook {
        String id
        String title
        String author
        int tagCount
    }
    List<LibraryBook> books = []
    List<String> ideaTitles = []
    String errorMessage
}
