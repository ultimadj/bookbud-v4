package com.isajohnson.bookbudv4.model

/**
 * Created by David on 3/7/14.
 */
class Idea {
    String id
    String libraryId
    String summary
    List<String> tags
    List<String> bookIds
    Date createdDate
    Date modifiedDate
}
