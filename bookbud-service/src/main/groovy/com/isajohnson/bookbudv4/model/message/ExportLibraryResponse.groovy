package com.isajohnson.bookbudv4.model.message

import com.isajohnson.bookbudv4.model.Book
import com.isajohnson.bookbudv4.model.Idea
import com.isajohnson.bookbudv4.model.Library
import groovy.transform.Canonical

/**
 * Created by David on 3/24/2014.
 */
@Canonical
class ExportLibraryResponse {
    Library library
    List<Book> books = []
    List<Idea> ideas = []
}
