package com.isajohnson.bookbudv4.model.message

import com.isajohnson.bookbudv4.model.User

/**
 * Created by David on 3/5/14.
 */
class UserResponse {
    String errorMessage
    User user
}
