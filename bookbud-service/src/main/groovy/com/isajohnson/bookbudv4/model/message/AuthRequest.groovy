package com.isajohnson.bookbudv4.model.message

import groovy.transform.Canonical

/**
 * Created by David on 3/24/2014.
 */
@Canonical
class AuthRequest {
    String user
    String pass
}
