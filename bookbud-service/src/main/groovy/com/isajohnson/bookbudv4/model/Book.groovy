package com.isajohnson.bookbudv4.model

/**
 * Created by David on 3/7/14.
 */
class Book {
    String id
    String libraryId
    String title
    String author
    String notes
    List<String> tags
    List<String> ideaIds
    Date createdDate
    Date modifiedDate


    @Override
    String toString() {
        return "Book{" +
                "id='" + id + '\'' +
                ", libraryId='" + libraryId + '\'' +
                ", title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", notes='" + notes + '\'' +
                ", tags=" + tags +
                ", ideaIds=" + ideaIds +
                ", createdDate=" + createdDate +
                ", modifiedDate=" + modifiedDate +
                '}';
    }
}
