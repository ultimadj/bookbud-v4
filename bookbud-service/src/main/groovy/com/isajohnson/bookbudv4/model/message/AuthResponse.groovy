package com.isajohnson.bookbudv4.model.message

/**
 * Created by David on 3/1/14.
 */
class AuthResponse {
    String token
    String name
    boolean success
    int secondsTilExpiration
}
