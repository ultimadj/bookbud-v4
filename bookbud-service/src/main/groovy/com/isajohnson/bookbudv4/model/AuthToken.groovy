package com.isajohnson.bookbudv4.model

/**
 * Created by David on 3/1/14.
 */
class AuthToken {
    String id
    String userId
    String token
    Date expiration
}
