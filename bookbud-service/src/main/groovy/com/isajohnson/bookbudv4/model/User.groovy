package com.isajohnson.bookbudv4.model

/**
 * Created by David on 2/25/14.
 */
class User {
    String id
    String name
    String email
    String encryptedPassword
}
