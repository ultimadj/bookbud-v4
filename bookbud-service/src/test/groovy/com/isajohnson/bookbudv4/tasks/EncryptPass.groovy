package com.isajohnson.bookbudv4.tasks

import org.jasypt.util.password.StrongPasswordEncryptor

/**
 * Created by David on 3/5/14.
 */
class EncryptPass {
    static void main(String[] args) {
        StrongPasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor()
        String result = passwordEncryptor.encryptPassword("flagnod2014")

        StrongPasswordEncryptor passwordEncryptor2 = new StrongPasswordEncryptor()
        String result2 = passwordEncryptor2.encryptPassword("test")

        println(result)
        println(result2)
        println(result == result2)
        println(passwordEncryptor2.checkPassword("test", result))
        println(passwordEncryptor2.checkPassword("test", result2))
        println(passwordEncryptor2.checkPassword("test2", result2))
    }
}
