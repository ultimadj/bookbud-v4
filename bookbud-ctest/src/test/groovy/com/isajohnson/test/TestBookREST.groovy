package com.isajohnson.test

import groovyx.net.http.HttpResponseDecorator
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.testng.annotations.BeforeClass
import org.testng.annotations.Test
import groovyx.net.http.RESTClient
import static org.testng.Assert.*

/**
 * Created by David on 3/8/14.
 */
class TestBookREST {
    Logger log = LoggerFactory.getLogger(TestBookREST.class)
    RESTClient client
    String token
    @BeforeClass void initData() {
        client = new RESTClient("http://localhost:8080/rest/")
        HttpResponseDecorator initResponse = client.get(path: "init");
        log.info("Init response: {}", initResponse.data)
        assertEquals(initResponse.status, 200)

        HttpResponseDecorator authResponse = client.get(path: "auth", query: [user: "a@b.com", pass: "test"])
        log.info("Auth response: {}", authResponse.data)
        assertEquals(authResponse.status, 200)
        assertTrue(authResponse.data.success)
        token = authResponse.data.token
    }

    @Test void firstTest() {
        HttpResponseDecorator libraryResponse = client.get(
                path: "library",
                query: [token: token]
        )
        log.info("Library response: {}", libraryResponse.data)
        assertEquals(libraryResponse.status, 200)
        assertNull(libraryResponse.data.errorMessage)
        assertTrue(libraryResponse.data.books.size > 0)
    }
}
